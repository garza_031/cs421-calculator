﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Calculator
{



    public partial class MainPage : ContentPage
    {
        string x = "";

        void Handle_Clicked7(object sender, System.EventArgs e)
        {
             x += "7";
            display.Text = x;
        }

        void Handle_Clicked8(object sender, System.EventArgs e)
        {
            x += "8";
            display.Text = x;
        }

        void Handle_Clicked9(object sender, System.EventArgs e)
        {
            x += "9";
            display.Text = x;
        }

        void Handle_Clickedplus(object sender, System.EventArgs e)
        {
            x += "+";
            display.Text = x;
        }

        void Handle_Clicked4(object sender, System.EventArgs e)
        {
            x += "4";
            display.Text = x;
        }

        void Handle_Clicked5(object sender, System.EventArgs e)
        {
            x += "5";
            display.Text = x;
        }

        void Handle_Clicked6(object sender, System.EventArgs e)
        {
            x += "6";
            display.Text = x;
        }

        void Handle_Clickedminus(object sender, System.EventArgs e)
        {
            x += "-";
            display.Text = x;
        }

        void Handle_Clicked1(object sender, System.EventArgs e)
        {
            x += "1";
            display.Text = x;
        }

        void Handle_Clicked2(object sender, System.EventArgs e)
        {
            x += "2";
            display.Text = x;
        }

        void Handle_Clicked3(object sender, System.EventArgs e)
        {
            x += "3";
            display.Text = x;
        }

        void Handle_Clickedmult(object sender, System.EventArgs e)
        {
            x += "*";
            display.Text = x;
        }

        void Handle_Clickedc(object sender, System.EventArgs e)
        {
            x = "0";
            display.Text = x;
        }

        void Handle_Clicked0(object sender, System.EventArgs e)
        {
            x += "0";
            display.Text = x;
        }

        void Handle_Clickedeq(object sender, System.EventArgs e)
        {
            x += "=";
            display.Text = x;
        }

        void Handle_Clickeddiv(object sender, System.EventArgs e)
        {
            x += "/";
            display.Text = x;
        }

        public MainPage()
        {
            InitializeComponent();
        }
    }
}
